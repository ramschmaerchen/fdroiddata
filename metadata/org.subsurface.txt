Categories:Science & Education
License:GPL
Web Site:http://subsurface.hohndel.org
Source Code:http://git.hohndel.org/index.cgi?p=subsurface.git;a=summary
Issue Tracker:http://trac.hohndel.org/report/1

Auto Name:Subsurface
Summary:Dive logger
Description:
Companion app for the cross-platform Subsurface desktop app

* Capture dive positions
* Search and modify old dives
* Synchronise dives with a server for manipulation in desktop app
.

Repo Type:git
Repo:git://subsurface.hohndel.org/subsurface-companion.git

Build:1.7,8
    commit=1.7
    maven=yes
    prebuild=sed -i '/sherlock/d' project.properties

Build:2.0,11
    disable=beta release
    commit=2.0b2

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0
Current Version Code:12

