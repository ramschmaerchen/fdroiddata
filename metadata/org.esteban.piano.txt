Categories:Multimedia
License:GPLv3
Web Site:https://gitorious.org/piano
Source Code:https://gitorious.org/piano/piano
Issue Tracker:

Auto Name:Piano
Summary:Virtual piano keyboard
Description:
Basic virtual piano based on Hexiano.
.

Repo Type:git
Repo:https://git.gitorious.org/piano/piano.git

Build:1.0,1
    commit=62c7fde
    srclibs=1:appcompat@v7
    prebuild=cp libs/android-support-v4.jar $$appcompat$$/libs/

Build:1.1,2
    commit=c69e84b
    srclibs=1:appcompat@v7
    prebuild=cp libs/android-support-v4.jar $$appcompat$$/libs/

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1
Current Version Code:2

