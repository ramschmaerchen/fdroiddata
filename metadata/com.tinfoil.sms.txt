Categories:Phone & SMS
License:GPLv3
Web Site:http://tinfoilhat.github.io/tinfoil-sms
Source Code:https://github.com/tinfoilhat/tinfoil-sms
Issue Tracker:https://github.com/tinfoilhat/tinfoil-sms/issues

Auto Name:Tinfoil-SMS
Summary:Encrypt text messages
Description:
Tinfoil-SMS encrypts your texts. It uses 256 bit ECC public keys as well
as a unique signed key exchange to prevent any "man-in-the-middle" attacks.
.

Repo Type:git
Repo:https://github.com/tinfoilhat/tinfoil-sms.git

Build:1.3.1,16
    disable=orwell and bouncycastle/strippedcastle fail to build
    commit=1.3.1-fdroid
    rm=libs/*
    scanignore=resources
    extlibs=android/android-support-v4.jar
    srclibs=1:NineOldAndroids@2.4.0,Orwell@v1.1,BouncyCastle@r1rv50
    prebuild=cp $$Orwell$$/libs/bcprov-jdk15on-150.jar $$Orwell$$/../bin/orwell-1.1.jar libs/

Maintainer Notes:

bcprov cannot be replaced with..
 .. bouncycastle sourcebuild
 .. bouncycastle jar release
 .. strippedcastle sourcebuild

it currently works with bcprov jar provided by orwell

orwell itself fails to build
.

Auto Update Mode:Version +-fdroid %v
Update Check Mode:Tags
Current Version:1.3.1
Current Version Code:16

