Categories:Development
License:Apache2
Web Site:https://github.com/AndlyticsProject/andlytics
Source Code:https://github.com/AndlyticsProject/andlytics
Issue Tracker:https://github.com/AndlyticsProject/andlytics/issues

Auto Name:Andlytics
Summary:Track your apps on Play Store
Description:
View stats and reply to comments. Requires a google account on the phone but
not an admob account.

[http://htmlpreview.github.com/?https://github.com/AndlyticsProject/andlytics/blob/master/res/raw/changelog.html Release Notes].
.

Repo Type:git
Repo:https://github.com/AndlyticsProject/andlytics.git

Build:2.4.3,226
    commit=v2.4.3
    init=rm -f ant.properties

Build:2.5.1,228
    commit=v2.5.1
    init=rm -f ant.properties

Build:2.5.5,233
    commit=v2.5.5
    init=rm -f ant.properties

Build:2.5.6,238
    commit=v2.5.6
    init=rm -f ant.properties

Build:2.5.7,243
    commit=v2.5.7
    init=rm -f ant.properties

Build:2.5.8,247
    commit=v2.5.8
    init=rm -f ant.properties

Build:2.6.0,250
    disable=missing 'gps' subproject
    commit=v2.6.0
    init=rm -f ant.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.6.0
Current Version Code:250

