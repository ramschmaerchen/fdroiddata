Categories:Internet
License:GPLv3+
Web Site:http://www.ratebeer.com
Source Code:https://code.google.com/p/ratebeerforandroid/source
Issue Tracker:https://code.google.com/p/ratebeerforandroid/issues

Auto Name:RateBeer for Android
Summary:RateBeer.com client
Description:
No description available
.

Repo Type:hg
Repo:https://code.google.com/p/ratebeerforandroid

Build:1.5.8,52
    disable=filled with jars, uses google play services lib
    commit=none
    subdir=RateBeerForAndroid

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.5.8
Current Version Code:52

