Categories:Internet
License:AGPLv3
Web Site:http://openbmap.org
Source Code:https://code.google.com/p/openbmap/source
Issue Tracker:https://code.google.com/p/openbmap/issues

Auto Name:Radiobeacon
Summary:Contribute to coverage maps
Description:
Map wireless networks and cell towers for the
[http://openbmap.org openbmap.org] database.

openBmap is a free and open map of wireless communicating objects.
Collected data can be downloaded from the website and used for many purposes,
such as determining your own location without GPS or creating network coverage
maps.

Hint: Use long-press on a session to upload it to the server.

Status: Testing
.

Repo Type:git
Repo:https://code.google.com/p/openbmap

Build:1.0,1
    disable=wrongly versioned (was fe1f5fe76b)
    commit=unknown - see disabled

Build:0.7.1,2
    commit=858b0d926c14
    subdir=android
    init=sed -i '/proguard/d' project.properties
    srclibs=Mapsforge@0f71a427bd50
    build=$$MVN3$$ clean package -DskipTests -f $$Mapsforge$$/pom.xml && \
        mv $$Mapsforge$$/mapsforge-map/target/mapsforge-map-0.3.1-SNAPSHOT-jar-with-dependencies.jar libs/

Build:0.7.6,5
    commit=v0.7.6
    subdir=android
    prebuild=sed -i '/proguard/d' project.properties

Build:0.7.7,6
    commit=v0.7.7
    subdir=android
    prebuild=sed -i '/proguard/d' project.properties

Build:0.7.7,7
    commit=v0.7.7
    subdir=android
    prebuild=sed -i '/proguard/d' project.properties

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.7.7
Current Version Code:7

